=begin

This class is used for Token Billing.
Token Billing is made of two phases:

1. Setup Phase
2. Rebill Phase

The setup phase is done using another method; PxFusion. It returns a BillingId
The rebill phase is done using PxPost, in this class. It charges the stored Credit Card.

=end

module Dps
  class PxPost
    attr_accessor :username, :password, :merchant_reference, :endpoint

    def initialize(options={})
      if options[:test_mode]
        self.username = options[:username] || ENV['TEST_DPS_PXPOST_USERNAME'] || ENV['DPS_PXPOST_USERNAME']
        self.password = options[:password] || ENV['TEST_DPS_PXPOST_PASSWORD'] || ENV['DPS_PXPOST_PASSWORD']
      else
        self.username = options[:username] || ENV['DPS_PXPOST_USERNAME']
        self.password = options[:password] || ENV['DPS_PXPOST_PASSWORD']
      end

      self.merchant_reference = options[:merchant_reference] || ENV['DPS_PXPOST_REFERENCE']
      self.endpoint = 'https://sec.paymentexpress.com/pxpost.aspx'
    end

    def rebill_card amount, dps_billing_id, txn_data_1=nil, txn_data_2=nil, txn_data_3=nil
      raw_response = call_client amount: amount, dps_billing_id: dps_billing_id, txn_data_1: txn_data_1, txn_data_2: txn_data_2, txn_data_3: txn_data_3
      response = parse_client_response raw_response
      PxPostTransaction.new response
    end



    private



    def call_client details
      RestClient.post(endpoint, raw_xml(details), :content_type => :xml, :accept => :xml).body
    end

    def parse_client_response response
      parser = Nori.new(:convert_tags_to => lambda { |tag| tag.snakecase.to_sym })
      hash = parser.parse(response)
      hash[:txn]
    end

    def raw_xml details
<<XML
<Txn>
  <PostUsername>#{username}</PostUsername>
  <PostPassword>#{password}</PostPassword>
  <Amount>#{"%.2f" % details[:amount]}</Amount>
  <InputCurrency>NZD</InputCurrency>
  <TxnType>Purchase</TxnType>
  <TxnData1>#{details[:txn_data_1].try(:html_safe)}</TxnData1>
  <TxnData2>#{details[:txn_data_2].try(:html_safe)}</TxnData2>
  <TxnData3>#{details[:txn_data_3].try(:html_safe)}</TxnData3>
  <DpsBillingId>#{details[:dps_billing_id]}</DpsBillingId>
  <EnableAddBillCard>0</EnableAddBillCard>
  <MerchantReference>#{merchant_reference}</MerchantReference>
</Txn>
XML
    end

  end



  class PxPostTransaction
    attr_accessor :response

    def initialize response
      self.response = response
    end

    def is_success?
      return false unless response.present?
      response[:success] == "1"
    end

    def is_declined?
      response[:status] == "1"
    end

    def dps_transaction_reference
      response[:dps_txn_ref]
    end

    def received_at
      Time.parse response[:transaction][:rx_date_local]
    end

    def errors
      return [] if is_success?
      ['declined or unsuccessful']
    end
  end

end
