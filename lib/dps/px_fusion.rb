=begin

This class has 2 functions.

1. get_transaction_id
2. get_transaction

Use get_transaction_id to initialise the transaction, then use a form to post credit card details to DPS.
The final step uses get_transaction to check the transaction was completed successfully.

=end

require 'savon'

module Dps
  class PxFusion
    attr_accessor :username, :password, :merchant_reference, :client

    def initialize(options={})
      if options[:test_mode]
        self.username = options[:username] || ENV['TEST_DPS_FUSION_USERNAME'] || ENV['DPS_FUSION_USERNAME']
        self.password = options[:password] || ENV['TEST_DPS_FUSION_PASSWORD'] || ENV['DPS_FUSION_PASSWORD']
      else
        self.username = options[:username] || ENV['DPS_FUSION_USERNAME']
        self.password = options[:password] || ENV['DPS_FUSION_PASSWORD']
      end
      self.merchant_reference = options[:merchant_reference] || ENV['DPS_FUSION_REFERENCE']
      self.client = Savon.client(wsdl: 'https://sec.paymentexpress.com/pxf/pxf.svc?wsdl', log: !Rails.env.test?)
    end

    def get_transaction_id amount, return_url, options={}
      raw_response = call_client(:get_transaction_id, {amount: amount, return_url: return_url}.merge(options) )
      parse_client_response(:get_transaction_id, raw_response)[:session_id]
    end

    def get_card_transaction_id return_url, options={}
      raw_response = call_client(:get_transaction_id, {amount: '1.00', return_url: return_url, txn_type: 'Auth', enable_add_bill_card: true}.merge(options))
      parse_client_response(:get_transaction_id, raw_response)[:session_id]
    end

    def get_transaction session_id
      raw_response = call_client(:get_transaction, {session_id: session_id})
      response = parse_client_response :get_transaction, raw_response
      PxFusionTransaction.new response
    end

    private

    def call_client operation, details
      client.call(operation, send("#{operation}_payload", details)).body
    end

    def parse_client_response operation, response
      response["#{operation}_response".to_sym]["#{operation}_result".to_sym]
    end

    def get_transaction_id_payload details
      raise "amount is required" unless details[:amount]
      raise "return_url is required" unless details[:return_url]

      details[:txn_type] ||= 'Purchase'
      details[:enable_add_bill_card] ||= false

      transaction_detail = {
        txn_type: details[:txn_type],
        currency: 'NZD',
        merchant_reference: merchant_reference,
        enable_add_bill_card: details[:enable_add_bill_card].to_s,
        amount: sprintf('%#.2f', details[:amount]),
        return_url: details[:return_url],
        txn_data_1: details[:txn_data_1] || nil,
        txn_data_2: details[:txn_data_2] || nil,
        txn_data_3: details[:txn_data_3] || nil,
      }

      if details.has_key?(:txn_ref)
        transaction_detail['txn_ref'] = details['txn_ref'].to_s.truncate(16, omission: '')
      end


      # # If DPS was any good, we could do this. But it fails with no errors,
      # #  so until it gets better, we will use raw XML
      # return { message: { username: username, password: password, tran_detail: transaction_detail } }

      if details.has_key?(:txn_ref)
        transaction_reference_xml = "<txn_ref>#{transaction_detail[:txn_ref]}</txn_ref>"
      else
        transaction_reference_xml = ''
      end

      raw_xml = <<XML
<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
  <s:Body>
  <GetTransactionId xmlns="http://paymentexpress.com">
    <username>#{username}</username>
    <password>#{password}</password>
    <tranDetail xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
      <amount>#{transaction_detail[:amount]}</amount>
      <currency>#{transaction_detail[:currency]}</currency>
      <enableAddBillCard>#{transaction_detail[:enable_add_bill_card]}</enableAddBillCard>
      <merchantReference>#{transaction_detail[:merchant_reference]}</merchantReference>
      <returnUrl>#{transaction_detail[:return_url]}</returnUrl>
      <txnData1>#{transaction_detail[:txn_data_1].try(:html_safe)}</txnData1>
      <txnData2>#{transaction_detail[:txn_data_2].try(:html_safe)}</txnData2>
      <txnData3>#{transaction_detail[:txn_data_3].try(:html_safe)}</txnData3>
      <txnType>#{transaction_detail[:txn_type]}</txnType>#{transaction_reference_xml}
    </tranDetail>
  </GetTransactionId>
  </s:Body>
</s:Envelope>
XML
      { xml: raw_xml }
    end

    def get_transaction_payload details
      raise "session_id is required" unless details[:session_id]

      # # If DPS was any good, we could do this. But it fails with no errors,
      # #  so until it gets better, we will use raw XML
      # return { message: { username: username, password: password, session_id: session_id } }

      raw_xml = <<XML
<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
  <s:Body>
  <GetTransaction xmlns="http://paymentexpress.com">
    <username>#{username}</username>
    <password>#{password}</password>
    <transactionId>#{details[:session_id]}</transactionId>
    <sessionId>#{details[:session_id]}</sessionId>
  </GetTransaction>
  </s:Body>
</s:Envelope>
XML
      { xml: raw_xml }
    end

  end




  class PxFusionTransaction
    attr_accessor :response

    def initialize response
      self.response = response
    end

    def is_success?
      response[:status] == "0"
    end

    def is_declined?
      response[:status] == "1"
    end

    def error_message
      message = response[:response_text].try(:titlecase)
      message ||= "responded with non-success status"
      message
    end
  end

end
