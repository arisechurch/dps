Gem::Specification.new do |s|
  s.name        = 'dps'
  s.version     = '0.0.7'
  s.date        = '2016-02-02'
  s.summary     = "DPS"
  s.description = "A ruby DPS PxFusion & PxPost API implementation."
  s.authors     = ["Cameron Fowler", "Michael Brown"]
  s.email       = 'michael.brown@arisechurch.com'
  s.files       = ["lib/dps/px_fusion.rb", "lib/dps/px_post.rb"]
  s.homepage    =
    'http://gitlab.com/arisechurch/dps'
  s.license       = 'None'
  s.add_dependency('savon', '~> 2.0')
  s.add_dependency('rest-client', '~> 1.8')
end
